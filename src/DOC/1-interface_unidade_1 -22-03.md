# Estratégia

*recursos*
- Ouro **(Utilizado para comprar equipamentos)**
- [Imagem Ouro](https://www.empiricus.com.br/wp-content/uploads/2016/02/Investir-em-Ouro.png)

- Comida **(Utilizado para alimentar os combatentes)**
- [Imagem Comida](https://png.pngtree.com/element_origin_min_pic/16/12/02/f143b8bc7d53fea9d9a8c50a6efbdc42.jpg)

*Exercito*
- Soldados
- Cavaleiros
- Arqueiros
- Lanceiros
- Quantidade de comida: 4500
- Quantidade de ouro : 15

*Soldado*
- Possui: Espada e Armadura
- Vida: 100
- Gasto de comida: 100
- Dano: 10
- Golpe especial: Sacrificio **(Quando tiver menos de 20 de vida ataca com o dobro de ataque, mas sofre dobro de dano)**
- [Imagem Soldado](https://vignette.wikia.nocookie.net/assassinscreed/images/c/cb/Guard-Continental-AC3.png/revision/latest?cb=20121114165227)

*Cavaleiro*
- Possui: Lança e Armadura
- Utiliza: Cavalo
- Vida: 150
- Gasto de comida: 200
- Dano: 15
- Golpe especial: Enfurecido **(A cada rodada com menos de 1/3 de vida, aumenta seu ataque)**
- [Imagem Cavaleiro](http://3.bp.blogspot.com/_lrnheGDims4/TCD9U3eZZ1I/AAAAAAAAFcU/aC1qM5-2Lrs/s1600/catafracto+parto.gif)

*Arqueiro*
- Possui: Arco e Armadura
- Vida: 100 
- Gasto de comida: 100
- Dano: 15
- Golpe especial: Flechas de Fogo **(Uma vez a cada dois ataques, tem o dobro de dano)**
- [Imagem Arqueiro](https://tse4.mm.bing.net/th?id=OIP.Z50X3brgDPUY9BiA2G7XOAHaM4&pid=15.1)

*Lanceiro*
- Possui: Lança e Armadura
- Vida: 150 
- Gasto de comida: 150
- Dano: 25
- Golpe especial: Recuar **(Quando perde metade da vida, recebe metade do dano)**
- [Imagem Lanceiro](http://redmount.weebly.com/uploads/3/1/7/3/31737141/293753666.png)

*Cavalo*
- Função: Auxiliar de cavaleiro.
- Dano: 5
- [Imagem Cavalo](http://www.infoescola.com/wp-content/uploads/2008/05/cavalo.jpg)

## Dificuldades
- Gerenciamento do tempo
- Leve dificuldade na parte dos commits

## Vídeo de demonstração do Software
- [Link Google Drive](https://drive.google.com/open?id=139BpppXQUfRay_beiUx8COCxLD4bDcMc)

## Material Utilizado
- [markdown support](http://plugins.netbeans.org/plugin/50964/markdown-support)
- [markdown medium](https://medium.com/@taylorhxu/markdown-for-dummies-a24e982b8e85)
- [aprenda markdown](https://blog.da2k.com.br/2015/02/08/aprenda-markdown/)
