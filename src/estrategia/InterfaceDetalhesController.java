package estrategia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Classe controladora de interfaceDetalhes.fxml
 *
 * @author Daniel e Guilherme
 */
public class InterfaceDetalhesController implements Initializable {

    private static int qtdOuro = 15;

    private static int[][] nivel = new int[4][2];
    @FXML
    private Label vidaArq, danoArq, vidaCav, danoCav, vidaLan, danoLan, vidaSol, danoSol, ouro;
    @FXML
    private Label defArq, defCav, defLan, defSol, ataArq, ataCav, ataLan, ataSol;

    /** Método para voltar à tela anterior.
     *
     */
    @FXML
    public void voltar() {
        main.trocaTela("Interface.fxml");
    }

    /** Método que retorna a quantidade de ouro.
     *
     * @return int - Quantidade de ouro.
     */
    public static int getQtdOuro() {
        return qtdOuro;
    }

    /** Método que modifica a quantidade de ouro.
     *
     * @param qtdOuro int - Quantidade de ouro.
     */
    public static void setQtdOuro(int qtdOuro) {
        InterfaceDetalhesController.qtdOuro = qtdOuro;
    }

    /** Método que retorna a matriz de níveis dos itens.
     *
     * @return int[][] - Níveis dos itens
     */
    public static int[][] getNivel() {
        return nivel;
    }

    /** Método para passar para a próxima tela.
     *
     */
    @FXML
    private void handleButtonAction() {
        main.trocaTela("Mapa.fxml");
    }

    /** Método para zerar os níveis dos itens.
     *
     */
    @FXML
    private void zerar() {
        for (int i = 0; i < 4; i++) {
            nivel[i][0] = 0;
            nivel[i][1] = 0;
        }
        qtdOuro = 15;
        atualizar();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizar();
    }

    /** Método para atualizar todos os labels da tela.
     *
     */
    public void atualizar() {
        ouro.setText("Ouro disponivel: " + InterfaceDetalhesController.getQtdOuro());
        vidaArq.setText("" + (InterfaceController.getQtd()[0] * 100));
        danoArq.setText("" + (InterfaceController.getQtd()[0] * 15));
        vidaCav.setText("" + (InterfaceController.getQtd()[1] * 150));
        danoCav.setText("" + (InterfaceController.getQtd()[1] * 15));
        vidaLan.setText("" + (InterfaceController.getQtd()[2] * 150));
        danoLan.setText("" + (InterfaceController.getQtd()[2] * 25));
        vidaSol.setText("" + (InterfaceController.getQtd()[3] * 100));
        danoSol.setText("" + (InterfaceController.getQtd()[3] * 15));
        ataArq.setText("" + (nivel[0][0]));
        ataCav.setText("" + (nivel[1][0]));
        ataLan.setText("" + (nivel[2][0]));
        ataSol.setText("" + (nivel[3][0]));
        defArq.setText("" + (nivel[0][1]));
        defCav.setText("" + (nivel[1][1]));
        defLan.setText("" + (nivel[2][1]));
        defSol.setText("" + (nivel[3][1]));
    }

    /** Método para passar para a tela do arqueiro.
     *
     */
    @FXML
    public void verArqueiro() {
        ArqueiroController.setQtd(InterfaceController.getQtd()[0]);
        main.trocaTela("Arqueiro.fxml");
    }

    /** Método para passar para a tela do cavaleiro.
     *
     */
    @FXML
    public void verCavaleiro() {
        CavaleiroController.setQtd(InterfaceController.getQtd()[1]);
        main.trocaTela("Cavaleiro.fxml");
    }

    /** Método para passar para a tela do lanceiro.
     *
     */
    public void verLanceiro() {
        LanceiroController.setQtd(InterfaceController.getQtd()[2]);
        main.trocaTela("Lanceiro.fxml");
    }

    /** Método para passar para a tela do soldado.
     *
     */
    public void verSoldado() {
        SoldadoController.setQtd(InterfaceController.getQtd()[3]);
        main.trocaTela("Soldado.fxml");
    }

}
