package estrategia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Classe controladora de arqueiro.fxml
 *
 * @author Daniel e Guilherme
 */
public class ArqueiroController implements Initializable {

    @FXML
    private Label qtdOuro;
    @FXML
    private Label defesa, ataque;
    private static int qtdArq = 0;

    /**
     * Método que modifica a quantidade de arqueiros.
     *
     * @param qtd - int Quantidade de arqueiros.
     */
    public static void setQtd(int qtd) {
        qtdArq = qtd;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atuOuro();
        defesa.setText("" + InterfaceDetalhesController.getNivel()[0][1]);
        ataque.setText("" + InterfaceDetalhesController.getNivel()[0][0]);
    }

    /**
     * Método de confirmar mudanças.
     *
     */
    @FXML
    public void Confirmar() {
        main.trocaTela("InterfaceDetalhes.fxml");
        //salvar informações mudadas
    }

    /**
     * Método de aumentar nível da armadura.
     *
     */
    @FXML
    public void defP() {
        if (InterfaceDetalhesController.getQtdOuro() > 0 && InterfaceDetalhesController.getNivel()[0][1]<5) {
            InterfaceDetalhesController.getNivel()[0][1]++;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() - 1);
            defesa.setText("" + InterfaceDetalhesController.getNivel()[0][1]);
            atuOuro();
        }
    }

    /**
     * Método de diminuir nível da armadura.
     *
     */
    @FXML
    public void defM() {
        if (InterfaceDetalhesController.getNivel()[0][1] > 0) {
            InterfaceDetalhesController.getNivel()[0][1]--;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() + 1);
            defesa.setText("" + InterfaceDetalhesController.getNivel()[0][1]);
            atuOuro();
        }
    }

    /**
     * Método de aumentar nível da arma.
     *
     */
    @FXML
    public void ataP() {
        if (InterfaceDetalhesController.getQtdOuro() > 0 && InterfaceDetalhesController.getNivel()[0][0]<5) {
            InterfaceDetalhesController.getNivel()[0][0]++;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() - 1);
            ataque.setText("" + InterfaceDetalhesController.getNivel()[0][0]);
            atuOuro();
        }
    }

    /**
     * Método de diminuir nível da arma.
     *
     */
    @FXML
    public void ataM() {
        if (InterfaceDetalhesController.getNivel()[0][0] > 0) {
            InterfaceDetalhesController.getNivel()[0][0]--;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() + 1);
            ataque.setText("" + InterfaceDetalhesController.getNivel()[0][0]);
            atuOuro();
        }
    }

    /**
     * Método de atualizar label de quantidade ouro.
     *
     */
    public void atuOuro() {
        qtdOuro.setText("Ouro disponivel: " + InterfaceDetalhesController.getQtdOuro());
    }

}
