package estrategia;

import estrategia.model.Cavaleiro;
import estrategia.model.Arqueiro;
import estrategia.model.Exercito;
import estrategia.model.Soldado;
import estrategia.model.Personagem;
import estrategia.model.Lanceiro;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 * Classe controladora de batalha.fxml
 *
 * @author Daniel e Guilherme
 */
public class BatalhaController implements Initializable {

    private static int[] pc = new int[4];
    private static Exercito jogador;
    private static Exercito computador;
    private static String str = "";
    @FXML
    private Label aviso, aPerdido, aPerdeu, sPerdido, sPerdeu, lPerdido, lPerdeu, cPerdido, cPerdeu, resJ, resC;
    
    @FXML
    private TextArea saida;
    /**
     * Método que modifica as quantidades do computador.
     *
     * @param pc int[] - Vetor de quantidades do computador
     */
    public static void setPc(int pc[]) {
        BatalhaController.pc = pc;
    }

    /**
     * Método de colocar exércitos batalhar.
     *
     */
    public void batalhar() {
        int dano, i = 0, sizeJogador = jogador.getExercito().size(), sizeComputador = computador.getExercito().size();

        while (sizeJogador > 0 && sizeComputador > 0) {
            dano = jogador.getExercito().get(i % sizeJogador).atacar();
            if (computador.getExercito().get(i % sizeComputador).sofrerDano(dano)) {
                str+=("\nTurno " + (i + 1) + ": ");
                morto(computador, i % sizeComputador);
                computador.getExercito().remove(i % sizeComputador);
                sizeComputador--;
                str+=("inimigo morto. Restam " + sizeComputador + " combatentes no exército inimigo.\n");
                if (sizeComputador == 0) {
                    break;
                }
            }

            dano = computador.getExercito().get(i % sizeComputador).atacar();
            if (jogador.getExercito().get(i % sizeJogador).sofrerDano(dano)) {
                str+=("\nTurno " + (i + 1) + ": ");
                morto(jogador, i % sizeJogador);
                jogador.getExercito().remove(i % sizeJogador);
                sizeJogador--;
                str+=("aliado morto. Restam " + sizeJogador + " combatentes no exército aliado.\n");
            }
            i++;
        }

        if (sizeJogador == 0) {
            resJ.setText("Perdedor");
            resC.setText("Vencedor");
            aviso.setText("VOCÊ PERDEU!");
            str+=("\nExército aliado ficou sem combatentes.");
        } else {
            resJ.setText("Vencedor");
            resC.setText("Perdedor");
            aviso.setText("VOCÊ VENCEU!");
            str+=("\nExército inimigo ficou sem combatentes.");
        }
        saida.setText(saida.getText()+str);
    }

    /**
     * Método que atualiza os labels.
     *
     */
    public void atualizarMortes() {
        aPerdido.setText("Arqueiros mortos:\t" + (jogador.getQtdInicial()[0] - jogador.getQtd()[0]) + " de " + jogador.getQtdInicial()[0]);
        sPerdido.setText("Soldados mortos:\t" + (jogador.getQtdInicial()[3] - jogador.getQtd()[3]) + " de " + jogador.getQtdInicial()[3]);
        lPerdido.setText("Lanceiros mortos:\t" + (jogador.getQtdInicial()[2] - jogador.getQtd()[2]) + " de " + jogador.getQtdInicial()[2]);
        cPerdido.setText("Cavaleiros mortos:\t" + (jogador.getQtdInicial()[1] - jogador.getQtd()[1]) + " de " + jogador.getQtdInicial()[1]);
        aPerdeu.setText("Arqueiros mortos:\t" + (computador.getQtdInicial()[0] - computador.getQtd()[0]) + " de " + computador.getQtdInicial()[0]);
        sPerdeu.setText("Soldados mortos:\t" + (computador.getQtdInicial()[3] - computador.getQtd()[3]) + " de " + computador.getQtdInicial()[3]);
        lPerdeu.setText("Lanceiros mortos:\t" + (computador.getQtdInicial()[2] - computador.getQtd()[2]) + " de " + computador.getQtdInicial()[2]);
        cPerdeu.setText("Cavaleiros mortos:\t" + (computador.getQtdInicial()[1] - computador.getQtd()[1]) + " de " + computador.getQtdInicial()[1]);
    }

    /**
     * Método que retorna o exército do jogador.
     *
     * @return Exercito - Exército do jogador
     */
    public static Exercito getJogador() {
        return jogador;
    }

    /**
     * Método que modifica o exército do jogador.
     *
     * @param jogador Exercito - Exército do jogador
     */
    public static void setJogador(Exercito jogador) {
        BatalhaController.jogador = jogador;
    }

    /**
     * Método que retorna o exército do computador.
     *
     * @return Exercito - Exército do computador
     */
    public static Exercito getComputador() {
        return computador;
    }

    /**
     * Método que modifica o exército do computador.
     *
     * @param computador Exercito - Exército do computador
     */
    public static void setComputador(Exercito computador) {
        BatalhaController.computador = computador;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Personagem[] combatentes = new Personagem[4];
        int nivel[][] = InterfaceDetalhesController.getNivel();
        jogador = new Exercito(InterfaceController.getQtd());
        computador = new Exercito(pc);
        combatentes[0] = new Arqueiro(nivel[0][0], nivel[0][1]);
        combatentes[1] = new Cavaleiro(nivel[1][0], nivel[1][1]);
        combatentes[2] = new Lanceiro(nivel[2][0], nivel[2][1]);
        combatentes[3] = new Soldado(nivel[3][0], nivel[3][1]);
        jogador.add(combatentes);

        combatentes[0] = new Arqueiro(1, 3);
        combatentes[1] = new Cavaleiro(0, 3);
        combatentes[2] = new Lanceiro(0, 3);
        combatentes[3] = new Soldado(2, 3);
        computador.add(combatentes);
        saida.setText("Informações da batalha:");
        batalhar();
        atualizarMortes();
    }

    /**
     * Método que retorna para a segunda tela.
     *
     */
    @FXML
    private void voltar() {
        main.trocaTela("Interface.fxml");
    }

    /**
     * Método que atualiza a quantidade de personagens vivos.
     *
     * @param ex Exercito - exército que será modificado
     * @param ind int - personagem que morreu.
     */
    private void morto(Exercito ex, int ind) {
        if (ex.getExercito().get(ind) instanceof Arqueiro) {
            ex.getQtd()[0]--;
            str+=("Arqueiro ");
        }
        if (ex.getExercito().get(ind) instanceof Cavaleiro) {
            ex.getQtd()[1]--;
            str+=("Cavaleiro ");
        }
        if (ex.getExercito().get(ind) instanceof Lanceiro) {
            ex.getQtd()[2]--;
            str+=("Lanceiro ");
        }
        if (ex.getExercito().get(ind) instanceof Soldado) {
            ex.getQtd()[3]--;
            str+=("Soldado ");
        }
    }
}
