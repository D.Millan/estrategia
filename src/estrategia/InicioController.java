package estrategia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *  Classe controladora de inicio.fxml
 *
 * @author Daniel e Guilherme
 */
public class InicioController implements Initializable {

    /**
     * Método que passa para a próxima tela.
     */
    @FXML
    public void avancar() {
        main.trocaTela("Interface.fxml");
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
