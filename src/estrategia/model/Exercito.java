package estrategia.model;

import java.util.ArrayList;

/**
 * Classe exército que possui vários personagens.
 *
 * @author Daniel e Guilherme
 */
public class Exercito {

    private ArrayList<Personagem> exercito;
    private int[] qtd;
    private int[] qtdInicial;

    /**
     * Método que retorna o ArrayList de personagens.
     *
     * @return ArrayList - Lista personagens
     */
    public ArrayList<Personagem> getExercito() {
        return exercito;
    }

    /**
     * Método que retorna o vetor de quantidades.
     *
     * @return int[] - Vetor de quantidades
     */
    public int[] getQtd() {
        return qtd;
    }

    /**
     * Método que retorna o vetor de quantidades de quando o jogo começou.
     *
     * @return int[] - Vetor de quantidades do início do jogo
     */
    public int[] getQtdInicial() {
        return qtdInicial;
    }

    /**
     * Método que modifica o vetor de quantidades de quando o jogo começou.
     *
     * @param qtdInicial int[] - Vetor de quantidades do início do jogo
     */
    public void setQtdInicial(int[] qtdInicial) {
        this.qtdInicial = qtdInicial;
    }

    /**
     * Método que modifica o vetor de quantidades.
     *
     * @param qtd int[] - Vetor de quantidades
     */
    public void setQtd(int[] qtd) {
        this.qtd = qtd;
    }

    /**
     * Método que retorna a quantidade de quando o jogo começou.
     *
     * @return int - Quantidade de quando o jogo começou.
     */
    public int getInicial() {
        int inicial = (qtdInicial[0] + qtdInicial[1] + qtdInicial[2] + qtdInicial[3]);
        return inicial;
    }

    /**
     * Método construtor que define as quantidades do exército.
     *
     * @param qtd int[] - Vetor de quantidades
     */
    public Exercito(int[] qtd) {
        this.qtd = qtd.clone();
        this.qtdInicial = qtd.clone();
        exercito = new ArrayList();
    }

    /**
     * Método para adicionar personagens.
     *
     * @param p Personagem[] - Lista de 4 personagens diferentes.
     */
    public void add(Personagem[] p) {
        for (int x = 3; x >= 0; x--) {
            for (int i = 0; i < qtd[x]; i++) {
                exercito.add(p[x]);
            }
        }
    }

    /**
     * Método para retorna a quantidade de personagens do exército como String.
     *
     * @return String - quantidade de personagens
     */
    @Override
    public String toString() {
        return "" + exercito.size();
    }
}
