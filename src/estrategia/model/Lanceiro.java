package estrategia.model;

/**
 * Classe combatente do tipo Lanceiro que é especialização de Personagem. Tem
 * 150 de vida e 25 de dano.
 *
 * @author Daniel e Guilherme
 */
public class Lanceiro extends Personagem {

    /**
     * Método contrutor que define os atributos do lanceiro
     *
     * @param nivelArma int - Nivel da arma que o lanceiro receberá
     * @param nivelArmadura int - Nivel da armadura que o lanceiro receberá
     */
    public Lanceiro(int nivelArma, int nivelArmadura) {
        super.setVida(150);
        super.setDano(25);
        setArma(new Item(nivelArma));
        setArmadura(new Item(nivelArmadura));
    }

    /**
     * Método de ataque do lanceiro
     *
     * @return int - dano produzido pelo ataque
     */
    @Override
    public int atacar() {
        return (super.getDano() + getArma().getAdc());
    }

    /**
     * Método de sofrer dano do lanceiro
     *
     * @param dano int - Dano recebido pelo lanceiro
     * @return boolean - Se o lanceiro ainda está vivo
     */
    @Override
    public boolean sofrerDano(int dano) {
        dano = (dano - getArmadura().getAdc());
        if (getVida() < 75) {
            super.setVida(super.getVida() - (int) dano / 2);
        } else {
            super.setVida(super.getVida() - dano);
        }
        return super.getVida() <= 0;
    }
}
