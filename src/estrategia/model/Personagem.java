package estrategia.model;

/**Classe abstrata dos combatentes do jogo. Tem vida, dano, arma e armadura.
 *
 * @author Daniel e Guilherme
 */
public abstract class Personagem {
    private int vida;
    private int dano;
    private Item arma;
    private Item armadura;
    
    /** Método abstrato de ataque do personagem
     *
     * @return int - Dano causado pelo ataque
     */
    public abstract int atacar();
    
    /** Método abstrato de ataque sofrido pelo personagem
     *
     * @param dano int - dano sofrido
     * @return boolean - Se personagem está vivo
     */
    public abstract boolean sofrerDano(int dano);

    /** Método que retorna a vida do personagem
     *
     * @return int - Vida do personagem
     */
    public int getVida() {
        return vida;
    }

    /** Método que modifica a vida do personagem
     *
     * @param vida int - vida do personagem
     */
    public void setVida(int vida) {
        this.vida = vida;
    }

    /**  Método que retorna a arma do personagem
     *
     * @return Item - Arma do personagem
     */
    public Item getArma() {
        return arma;
    }

    /** Método que retorna a arma do personagem
     *
     * @return Item - Armadura do personagem
     */
    public Item getArmadura() {
        return armadura;
    }

    /** Método que retorna o dano padrão do personagem
     *
     * @return int - Dano personagem
     */
    public int getDano() {
        return dano;
    }

    /** Método que modifica o dano padrão do personagem
     *
     * @param dano int - Dano do personagem
     */
    public void setDano(int dano) {
        this.dano = dano;
    }

    /** Método que modifica a arma do personagem
     *
     * @param arma Item - Arma do personagem
     */
    public void setArma(Item arma) {
        this.arma = arma;
    }

    /** Método que modifica a armadura do personagem
     *
     * @param armadura Item - Armadura do personagem
     */
    public void setArmadura(Item armadura) {
        this.armadura = armadura;
    }
    
}
