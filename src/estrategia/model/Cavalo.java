package estrategia.model;

/**
 * Classe que auxilia a classe Cavaleiro, fornecendo mais poder de dano ao
 * personagem.
 *
 * @author Daniel e Guilherme
 */
public class Cavalo {

    private int dano;

    /**
     * Método construtor que define o dano do cavalo, que é 5.
     *
     */
    public Cavalo() {
        dano = 5;
    }

    /**
     * Método de ataque do cavalo
     *
     * @return int - dano que será adicionado ao dano do cavaleiro
     */
    public int atacar() {
        return dano;
    }
}
