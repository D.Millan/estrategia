package estrategia.model;

/**
 * Classe combatente do tipo Arqueiro que é especialização de Personagem. Tem
 * 100 de vida e 15 de dano.
 *
 * @author Daniel e Guilherme
 */
public class Arqueiro extends Personagem {

    private boolean flechaFogo = false;

    /**
     * Método contrutor que define os atributos do arqueiro
     *
     * @param nivelArma int - Nivel da arma que o arqueiro receberá
     * @param nivelArmadura int - Nivel da armadura que o arqueiro receberá
     */
    public Arqueiro(int nivelArma, int nivelArmadura) {
        super.setVida(100);
        super.setDano(15);
        setArma(new Item(nivelArma));
        setArmadura(new Item(nivelArmadura));
    }

    /**
     * Método de ataque do arqueiro
     *
     * @return int - dano produzido pelo ataque
     */
    @Override
    public int atacar() {
        if (flechaFogo) {
            flechaFogo = false;
            return (super.getDano() + getArma().getAdc()) * 2;
        } else {
            flechaFogo = true;
            return super.getDano() + getArma().getAdc();
        }
    }

    /**
     * Método de sofrer dano do arqueiro
     *
     * @param dano int - Dano recebido pelo arqueiro
     * @return boolean - Se o arqueiro ainda está vivo
     */
    @Override
    public boolean sofrerDano(int dano) {
        dano = (dano - getArmadura().getAdc());
        super.setVida(super.getVida() - dano);
        return super.getVida() <= 0;
    }
}
