package estrategia.model;

/**
 * Classe combatente do tipo Soldado que é especialização de Personagem. Tem 100
 * de vida e 10 de dano.
 *
 * @author Daniel e Guilherme
 */
public class Soldado extends Personagem {

    /**
     * Método contrutor que define os atributos do soldado
     *
     * @param nivelArma int - Nivel da arma que o soldado receberá
     * @param nivelArmadura int - Nivel da armadura que o soldado receberá
     */
    public Soldado(int nivelArma, int nivelArmadura) {
        super.setVida(100);
        super.setDano(10);
        setArma(new Item(nivelArma));
        setArmadura(new Item(nivelArmadura));
    }

    /**
     * Método de ataque do soldado
     *
     * @return int - dano produzido pelo ataque
     */
    @Override
    public int atacar() {
        if (super.getVida() < 20) {
            return (super.getDano() + getArma().getAdc()) * 2;
        } else {
            return super.getDano() + getArma().getAdc();
        }
    }

    /**
     * Método de sofrer dano do soldado
     *
     * @param dano int - Dano recebido pelo soldado
     * @return boolean - Se o soldado ainda está vivo
     */
    @Override
    public boolean sofrerDano(int dano) {
        dano = (dano - getArmadura().getAdc());
        if (super.getVida() < 20) {
            super.setVida(super.getVida() - dano * 2);
        } else {
            super.setVida(super.getVida() - dano);
        }
        return super.getVida() <= 0;
    }

}
