package estrategia.model;

/**
 * Classe ferramenta que irá auxiliar os combatentes
 *
 * @author Daniel e Guilherme
 */
public class Item {

    private int adc;

    /**
     * Método construtor de definirá o nível do item
     *
     * @param nivel int - Nivel do item
     */
    public Item(int nivel) {
        adc = nivel;
    }

    /**
     * Método que retorna o adicional que o item concede
     *
     * @return int - adicional do item
     */
    public int getAdc() {
        return adc;
    }

    /**
     * Método que modifica o adicional que o item concede
     *
     * @param adc int - adicional do item
     */
    public void setAdc(int adc) {
        this.adc = adc;
    }

}
