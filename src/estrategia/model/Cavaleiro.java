package estrategia.model;

/**
 * Classe combatente do tipo Cavaleiro que é especialização de Personagem. Tem
 * 100 de vida e 15 de dano.
 *
 * @author Daniel e Guilherme
 */
public class Cavaleiro extends Personagem {

    private Cavalo cavalo = new Cavalo();

    /**
     * Método contrutor que define os atributos do cavaleiro
     *
     * @param nivelArma int - Nivel da arma que o cavaleiro receberá
     * @param nivelArmadura int - Nivel da armadura que o cavaleiro receberá
     */
    public Cavaleiro(int nivelArma, int nivelArmadura) {
        super.setVida(150);
        super.setDano(15);
        setArma(new Item(nivelArma));
        setArmadura(new Item(nivelArmadura));

    }

    /**
     * Método que retorna o cavalo do cavaleiro
     *
     * @return Cavalo - Cavalo do cavaleiro
     */
    public Cavalo getCavalo() {
        return cavalo;
    }

    /**
     * Método que modifica o cavalo do cavaleiro
     *
     * @param cavalo Cavalo - Cavalo do cavaleiro
     */
    public void setCavalo(Cavalo cavalo) {
        this.cavalo = cavalo;
    }

    /**
     * Método de ataque do arqueiro
     *
     * @return int - dano produzido pelo ataque
     */
    @Override
    public int atacar() {
        return getDano() + getArma().getAdc() + cavalo.atacar();
    }

    /**
     * Método de sofrer dano do cavaleiro
     *
     * @param dano int - Dano recebido pelo cavaleiro
     * @return boolean - Se o cavaleiro ainda está vivo
     */
    @Override
    public boolean sofrerDano(int dano) {
        dano = (dano - getArmadura().getAdc());
        setVida(getVida() - dano);
        if (getVida() < 50) {
            setDano(getDano() + 1);
        }

        return super.getVida() <= 0;
    }
}
