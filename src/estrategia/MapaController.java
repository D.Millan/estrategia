package estrategia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * Classe controladora de mapa.fxml
 *
 * @author Daniel e Guilherme
 */
public class MapaController implements Initializable {

    private int vetor[] = new int[4];

    /**
     * Método que retorna para a terceira tela.
     *
     */
    @FXML
    public void voltar() {
        main.trocaTela("InterfaceDetalhes.fxml");
    }

    /**
     * Método que envia o exército A para a batalha.
     *
     */
    @FXML
    public void oponenteA() {
        vetor[0] = 80;
        vetor[1] = 80;
        vetor[2] = 80;
        vetor[3] = 80;
        BatalhaController.setPc(vetor);
        main.trocaTela("Batalha.fxml");
    }

    /**
     * Método que envia o exército B para a batalha.
     *
     */
    public void oponenteB() {
        vetor[0] = 150;
        vetor[1] = 30;
        vetor[2] = 60;
        vetor[3] = 150;
        BatalhaController.setPc(vetor);
        main.trocaTela("Batalha.fxml");
    }

    /**
     * Método que envia o exército C para a batalha.
     *
     */
    public void oponenteC() {
        vetor[0] = 50;
        vetor[1] = 150;
        vetor[2] = 30;
        vetor[3] = 50;
        BatalhaController.setPc(vetor);
        main.trocaTela("Batalha.fxml");
    }

    /**
     * Método que envia o exército D para a batalha.
     *
     */
    public void oponenteD() {
        vetor[0] = 150;
        vetor[1] = 30;
        vetor[2] = 100;
        vetor[3] = 90;
        BatalhaController.setPc(vetor);
        main.trocaTela("Batalha.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}
