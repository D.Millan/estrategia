package estrategia;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe main do programa
 *
 * @author Daniel e Guilherme
 */
public class main extends Application {

    private static Stage stage;

    /**
     * Método que retorna o stage do main.
     *
     * @return Stage - Stage do main
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * Método que troca a tela que será exibida.
     *
     * @param tela String - Caminho do arquivo FXML que será exibido
     */
    public static void trocaTela(String tela) {
        Parent root = null;
        try {
            root = FXMLLoader.load(main.class.getResource(tela));
        } catch (Exception e) {
            System.out.println("Verificar arquivo FXML" + e.toString());
        }
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Inicio.fxml"));

        Scene scene = new Scene(root);
        //observar essa linha = 
        main.stage = stage;

        stage.setScene(scene);
        stage.show();
    }

    /**
     * Método main do programa
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
