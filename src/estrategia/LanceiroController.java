package estrategia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Classe controladora de lanceiro.fxml
 *
 * @author Daniel e Guilherme
 */
public class LanceiroController implements Initializable {

    private static int qtdLan = 0;

    @FXML
    private Label qtdOuro;

    @FXML
    private Label defesa, ataque;

    /**
     * Método de aumentar nível da armadura.
     *
     */
    @FXML
    public void defP() {
        if (InterfaceDetalhesController.getQtdOuro() > 0 && InterfaceDetalhesController.getNivel()[2][1]<5) {
            InterfaceDetalhesController.getNivel()[2][1]++;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() - 1);
            defesa.setText("" + InterfaceDetalhesController.getNivel()[2][1]);
            atuOuro();
        }
    }

    /**
     * Método de diminuir nível da armadura.
     *
     */
    @FXML
    public void defM() {
        if (InterfaceDetalhesController.getNivel()[2][1] > 0) {
            InterfaceDetalhesController.getNivel()[2][1]--;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() + 1);
            defesa.setText("" + InterfaceDetalhesController.getNivel()[2][1]);
            atuOuro();
        }
    }

    /**
     * Método de aumentar nível da arma.
     *
     */
    @FXML
    public void ataP() {
        if (InterfaceDetalhesController.getQtdOuro() > 0 && InterfaceDetalhesController.getNivel()[2][0]<5) {
            InterfaceDetalhesController.getNivel()[2][0]++;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() - 1);
            ataque.setText("" + InterfaceDetalhesController.getNivel()[2][0]);
            atuOuro();
        }
    }

    /**
     * Método de diminuir nível da arma.
     *
     */
    @FXML
    public void ataM() {
        if (InterfaceDetalhesController.getNivel()[2][0] > 0) {
            InterfaceDetalhesController.getNivel()[2][0]--;
            InterfaceDetalhesController.setQtdOuro(InterfaceDetalhesController.getQtdOuro() + 1);
            ataque.setText("" + InterfaceDetalhesController.getNivel()[2][0]);
            atuOuro();
        }
    }

    /**
     * Método de atualizar label de quantidade ouro.
     *
     */
    public void atuOuro() {
        qtdOuro.setText("Ouro disponivel: " + InterfaceDetalhesController.getQtdOuro());
    }

    /**
     * Método que modifica a quantidade de lanceiros.
     *
     *
     * @param qtd - Quantidade de lanceiros
     */
    public static void setQtd(int qtd) {
        qtdLan = qtd;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        qtdOuro.setText("Ouro disponivel: " + InterfaceDetalhesController.getQtdOuro());
        ataque.setText("" + InterfaceDetalhesController.getNivel()[2][0]);
        defesa.setText("" + InterfaceDetalhesController.getNivel()[2][1]);
    }

    /**
     * Método de confirmar mudanças.
     *
     */
    @FXML
    public void Confirmar() {
        main.trocaTela("InterfaceDetalhes.fxml");
        //salvar informações mudadas
    }

}
