package estrategia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Classe controladora de interface.fxml
 *
 * @author Daniel e Guilherme
 */
public class InterfaceController implements Initializable {

    private static int qtdComida;
    private static boolean iniciado = false;
    private static int[] qtd = new int[4];
    @FXML
    private Label qtdS, qtdC, qtdA, qtdL, comida;

    /** Método que retorna o vetor de quantidades de personagens.
     *
     * @return int[] - Quantidades de personagens
     */    
    public static int[] getQtd() {
        return qtd;
    }

    /** Método que modifica o vetor de quantidades de personagens.
     *
     * @param qtd int[] - Quantidades de personagens
     */
    public static void setQtd(int[] qtd) {
        InterfaceController.qtd = qtd;
    }
    
    /** Método que passa para a próxima tela.
     *
     */
    @FXML
    private void handleButtonAction(ActionEvent event) {
        main.trocaTela("InterfaceDetalhes.fxml");
    }

    /** Método que aumenta o número de soldados em 10.
     *
     */
    @FXML
    private void soldadoP(ActionEvent event) {
        if (qtdComida >= 100) {
            qtd[3] += 10;
            qtdComida -= 100;
            atuComida();
        }
        qtdS.setText("" + qtd[3]);
    }

    /** Método que diminui o número de soldados em 10.
     *
     */
    @FXML
    private void soldadoM(ActionEvent event) {
        if (qtd[3] > 0) {
            qtd[3] -= 10;
            qtdComida += 100;
            atuComida();
        }
        qtdS.setText("" + qtd[3]);
    }

    /** Método que aumenta o número de cavaleiros em 10.
     *
     */
    @FXML
    private void cavaleiroP(ActionEvent event) {
        if (qtdComida >= 200) {
            qtd[1] += 10;
            qtdComida -= 200;
            atuComida();
        }
        qtdC.setText("" + qtd[1]);
    }

    /** Método que diminui o número de cavaleiros em 10.
     *
     */
    @FXML
    private void cavaleiroM(ActionEvent event) {
        if (qtd[1] > 0) {
            qtd[1] -= 10;
            qtdComida += 200;
            atuComida();
        }
        qtdC.setText("" + qtd[1]);
    }

    /** Método que aumenta o número de arqueiros em 10.
     *
     */
    @FXML
    private void arqueiroP(ActionEvent event) {
        if (qtdComida >= 100) {
            qtd[0] += 10;
            qtdComida -= 100;
            atuComida();
        }
        qtdA.setText("" + qtd[0]);
    }

    /** Método que diminui o número de arqueiros em 10.
     *
     */
    @FXML
    private void arqueiroM(ActionEvent event) {
        if (qtd[0] > 0) {
            qtd[0] -= 10;
            qtdComida += 100;
            atuComida();
        }
        qtdA.setText("" + qtd[0]);
    }

    /** Método que aumenta o número de lanceiros em 10.
     *
     */
    @FXML
    private void lanceiroP(ActionEvent event) {
        if (qtdComida >= 150) {
            qtd[2] += 10;
            qtdComida -= 150;
            atuComida();
        }
        qtdL.setText("" + qtd[2]);
    }

    /** Método que diminui o número de lanceiros em 10.
     *
     */
    @FXML
    private void lanceiroM(ActionEvent event) {
        if (qtd[2] > 0) {
            qtd[2] -= 10;
            qtdComida += 150;
            atuComida();
        }
        qtdL.setText("" + qtd[2]);
    }

    /** Método que zera a quantidade de combatentes selecionados.
     *
     */
    @FXML
    private void zerar() {
        for (int i = 0; i < 4; i++) {
            qtd[i] = 0;
        }
        qtdComida = 4500;
        atuComida();
        qtdA.setText("" + qtd[0]);
        qtdC.setText("" + qtd[1]);
        qtdL.setText("" + qtd[2]);
        qtdS.setText("" + qtd[3]);
    }

    /** Método que atualiza o Label de comida disponível
     *
     */
    public void atuComida() {
        comida.setText("Comida disponivel: " + qtdComida);
    }

    /** Método de inicialização do controller.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (!iniciado) {
            zerar();
            iniciado = true;
        } else {
            atuComida();
            qtdA.setText("" + qtd[0]);
            qtdC.setText("" + qtd[1]);
            qtdL.setText("" + qtd[2]);
            qtdS.setText("" + qtd[3]);
        }
    }

}
